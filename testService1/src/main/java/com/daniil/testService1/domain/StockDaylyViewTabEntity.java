package com.daniil.testService1.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "stock_dayly_view_tab", schema = "public", catalog = "postgres")
public class StockDaylyViewTabEntity {
    private String id;
    private String firmaName;
    private String ticker;
    private String currency;
    private Long lastPrice;
    private Long lastClsPrice;
    private String chngPrice;
    private Long bestBid;
    private Long bestAsk;
    private String trades;
    private Long volume;
    private Long turnover;
    private String dateOriginal;

    @javax.persistence.Id
    @Column(name = "id", nullable = true, length = 20)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firma_name", nullable = true, length = 50)
    public String getFirmaName() {
        return firmaName;
    }

    public void setFirmaName(String firmaName) {
        this.firmaName = firmaName;
    }

    @Basic
    @Column(name = "ticker", nullable = true, length = 30)
    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    @Basic
    @Column(name = "currency", nullable = true, length = 10)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Basic
    @Column(name = "last_price", nullable = true, precision = 3)
    public Long getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(Long lastPrice) {
        this.lastPrice = lastPrice;
    }

    @Basic
    @Column(name = "last_cls_price", nullable = true, precision = 3)
    public Long getLastClsPrice() {
        return lastClsPrice;
    }

    public void setLastClsPrice(Long lastClsPrice) {
        this.lastClsPrice = lastClsPrice;
    }

    @Basic
    @Column(name = "chng_price", nullable = true, length = 15)
    public String getChngPrice() {
        return chngPrice;
    }

    public void setChngPrice(String chngPrice) {
        this.chngPrice = chngPrice;
    }

    @Basic
    @Column(name = "best_bid", nullable = true, precision = 3)
    public Long getBestBid() {
        return bestBid;
    }

    public void setBestBid(Long bestBid) {
        this.bestBid = bestBid;
    }

    @Basic
    @Column(name = "best_ask", nullable = true, precision = 3)
    public Long getBestAsk() {
        return bestAsk;
    }

    public void setBestAsk(Long bestAsk) {
        this.bestAsk = bestAsk;
    }

    @Basic
    @Column(name = "trades", nullable = true, length = 10)
    public String getTrades() {
        return trades;
    }

    public void setTrades(String trades) {
        this.trades = trades;
    }

    @Basic
    @Column(name = "volume", nullable = true, precision = 0)
    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    @Basic
    @Column(name = "turnover", nullable = true, precision = 3)
    public Long getTurnover() {
        return turnover;
    }

    public void setTurnover(Long turnover) {
        this.turnover = turnover;
    }

    @Basic
    @Column(name = "date_original", nullable = true, length = 15)
    public String getDateOriginal() {
        return dateOriginal;
    }

    public void setDateOriginal(String dateOriginal) {
        this.dateOriginal = dateOriginal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockDaylyViewTabEntity that = (StockDaylyViewTabEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firmaName, that.firmaName) &&
                Objects.equals(ticker, that.ticker) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(lastPrice, that.lastPrice) &&
                Objects.equals(lastClsPrice, that.lastClsPrice) &&
                Objects.equals(chngPrice, that.chngPrice) &&
                Objects.equals(bestBid, that.bestBid) &&
                Objects.equals(bestAsk, that.bestAsk) &&
                Objects.equals(trades, that.trades) &&
                Objects.equals(volume, that.volume) &&
                Objects.equals(turnover, that.turnover) &&
                Objects.equals(dateOriginal, that.dateOriginal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firmaName, ticker, currency, lastPrice, lastClsPrice, chngPrice, bestBid, bestAsk, trades, volume, turnover, dateOriginal);
    }
}
