package com.daniil.testService1.DTO;


public class FileDataDTO {


    private String firmaId;
    private String firmaname;
    private String ticker;
    private String currency;
    private String lastPrice;
    private String lastClsPrice;
    private String chngPrice;
    private String bestBid;
    private String bestAsk;
    private String trades;
    private Long volume;
    private String turnover;
    private String dateOriginal;
    private String ISIN;
    private String marketPlace;
    private String segment;
    private String avgPrice;


    public String getId() {
        return firmaId;
    }

    public void setId(String id) {
        this.firmaId = id;
    }

    public String getFirmaname() {
        return firmaname;
    }

    public void setFirmaname(String firmaname) {
        this.firmaname = firmaname;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(String lastPrice) {
        this.lastPrice = lastPrice;
    }

    public String getLastClsPrice() {
        return lastClsPrice;
    }

    public void setLastClsPrice(String lastClsPrice) {
        this.lastClsPrice = lastClsPrice;
    }

    public String getChngPrice() {
        return chngPrice;
    }

    public void setChngPrice(String chngPrice) {
        this.chngPrice = chngPrice;
    }

    public String getBestBid() {
        return bestBid;
    }

    public void setBestBid(String bestBid) {
        this.bestBid = bestBid;
    }

    public String getBestAsk() {
        return bestAsk;
    }

    public void setBestAsk(String bestAsk) {
        this.bestAsk = bestAsk;
    }

    public String getTrades() {
        return trades;
    }

    public void setTrades(String trades) {
        this.trades = trades;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    public String getDateOriginal() {
        return dateOriginal;
    }

    public void setDateOriginal(String dateOriginal) {
        this.dateOriginal = dateOriginal;
    }

    public String getISIN() {
        return ISIN;
    }

    public void setISIN(String ISIN) {
        this.ISIN = ISIN;
    }

    public String getMarketPlace() {
        return marketPlace;
    }

    public void setMarketPlace(String marketPlace) {
        this.marketPlace = marketPlace;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }


    public String getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(String avgPrice) {
        this.avgPrice = avgPrice;
    }

}
