package com.daniil.testService1.DTO;

import java.sql.Date;

public class DailyStockDTO {

    private String id;
    private String firmaname;
    private String ticker;
    private String currency;
    private Long lastPrice;
    private Long lastClsPrice;
    private String chngPrice;
    private Long bestBid;
    private Long bestAsk;
    private String trades;
    private Long volume;
    private Long turnover;
    private Date dateOriginal;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirmaname() {
        return firmaname;
    }

    public void setFirmaname(String firmaname) {
        this.firmaname = firmaname;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(Long lastPrice) {
        this.lastPrice = lastPrice;
    }

    public Long getLastClsPrice() {
        return lastClsPrice;
    }

    public void setLastClsPrice(Long lastClsPrice) {
        this.lastClsPrice = lastClsPrice;
    }

    public String getChngPrice() {
        return chngPrice;
    }

    public void setChngPrice(String chngPrice) {
        this.chngPrice = chngPrice;
    }

    public Long getBestBid() {
        return bestBid;
    }

    public void setBestBid(Long bestBid) {
        this.bestBid = bestBid;
    }

    public Long getBestAsk() {
        return bestAsk;
    }

    public void setBestAsk(Long bestAsk) {
        this.bestAsk = bestAsk;
    }

    public String getTrades() {
        return trades;
    }

    public void setTrades(String trades) {
        this.trades = trades;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Long getTurnover() {
        return turnover;
    }

    public void setTurnover(Long turnover) {
        this.turnover = turnover;
    }

    public Date getDateOriginal() {
        return dateOriginal;
    }

    public void setDateOriginal(Date dateOriginal) {
        this.dateOriginal = dateOriginal;
    }
}
