package com.daniil.testService1.service;


import com.daniil.testService1.domain.StockDaylyViewTabEntity;
import com.daniil.testService1.repository.StockDaylyViewRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockDaylyViewServiceImpl implements StockDaylyViewService {


    private final StockDaylyViewRepository stockDaylyViewRepository;

    public StockDaylyViewServiceImpl(StockDaylyViewRepository stockDaylyViewRepository) {
        this.stockDaylyViewRepository = stockDaylyViewRepository;
    }


    @Override
    public List<StockDaylyViewTabEntity> findAll() {
        List<StockDaylyViewTabEntity> stocks = new ArrayList<>();
        stockDaylyViewRepository.findAll().forEach(stocks::add);
        return stocks;
    }

    @Override
    public List<StockDaylyViewTabEntity> getAllStocks() {
        return null;
    }


//    @Override
//    public List<StockDaylyViewTabEntity> getByDateOriginal(String originalDate) {
//
//        List<StockDaylyViewTabEntity> stocks=new ArrayList<>();
//        stockDaylyViewRepository.getByDateOriginal(originalDate).forEach(stocks::add);
//        return stocks;
//    }


}
