package com.daniil.testService1.service;

import com.daniil.testService1.domain.StockDaylyViewTabEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StockDaylyViewService {


    public List<StockDaylyViewTabEntity> findAll();
    public List<StockDaylyViewTabEntity> getAllStocks();

//    List<StockDaylyViewTabEntity> getByDateOriginal(@Param("dateOriginal") String originalDate);




}
