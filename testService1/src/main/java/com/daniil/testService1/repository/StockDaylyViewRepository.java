package com.daniil.testService1.repository;

import com.daniil.testService1.domain.StockDaylyViewTabEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockDaylyViewRepository extends JpaRepository<StockDaylyViewTabEntity, String> {


    List<StockDaylyViewTabEntity> findAll();


//     List<StockDaylyViewTabEntity> findByDateOriginalAndVolumeExists(String originalDate);
//
//     @Query("SELECT stockDaylyViewTabEntity FROM StockDaylyViewTabEntity stockDaylyViewTabEntity WHERE stockDaylyViewTabEntity.dateOriginal = :dateOriginal order by stockDaylyViewTabEntity.volume DESC")
//     List<StockDaylyViewTabEntity> getByDateOriginal(@Param("dateOriginal") String originalDate);


}


