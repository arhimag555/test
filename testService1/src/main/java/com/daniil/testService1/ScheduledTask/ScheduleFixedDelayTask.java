package com.daniil.testService1.ScheduledTask;


import com.daniil.testService1.DTO.FileDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.daniil.testService1.ScheduledTask.DataFromUrlAsCSVtoDB.readDataFromWebToList;
import static com.daniil.testService1.ScheduledTask.DataFromUrlAsCSVtoDB.readListDataFromCSVToDb;
import static com.daniil.testService1.utils.Utils.*;

@Component
public class ScheduleFixedDelayTask {

    private static final Logger log = LoggerFactory.getLogger(ScheduleFixedDelayTask.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    public static List<FileDataDTO> dataFromCSV = new ArrayList<>();

    //here we need to put 30min= 30 x 60 000 = 1800000
    @Scheduled(fixedRate = 120000)
    public void loadDataFromUrlAsCSVtoOurDB() throws IOException {
        log.info("Starting schedule task ....");
        log.info("The time is now {}", dateFormat.format(new Date()));
        String dateTodayForUrl = todayDateForURL();
        log.info("Today is: " + dateTodayForUrl);

        LinkedHashMap<Integer, String> tenDays = gen10Days();


        try {

            //Read all 10 days CSV files to list.
            tenDays.forEach((key, value) -> readDataFromWebToList(value));

            //reading List<FileDataDTO> elements to db (if exists --> update, if no --> insert)
            readListDataFromCSVToDb(dateTodayForUrl);


        } catch (Exception e) {
            log.error("Error in loading/parsing data from URL ", e);
            e.printStackTrace();
        }


        log.info("Scheduled task finished  ...");

    }


}
