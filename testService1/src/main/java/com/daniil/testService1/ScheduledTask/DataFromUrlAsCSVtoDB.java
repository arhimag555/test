package com.daniil.testService1.ScheduledTask;

import com.daniil.testService1.DTO.FileDataDTO;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.SQLException;

import static com.daniil.testService1.ScheduledTask.ScheduleFixedDelayTask.dataFromCSV;
import static com.daniil.testService1.utils.Utils.*;

public class DataFromUrlAsCSVtoDB {


    private static final Logger log = LoggerFactory.getLogger(DataFromUrlAsCSVtoDB.class);

    public static void readDataFromWebToList(String date) {


        try {

            log.info("Reading data from URL  https://www.nasdaqbaltic.com/market....");
            URL stockURL = new URL("https://www.nasdaqbaltic.com/market/?pg=mainlist&market=&date=" + date + "&lang=en&downloadcsv=1&csv_style=Baltic");
            BufferedReader in = new BufferedReader(new InputStreamReader(stockURL.openStream()));
            CSVReader reader = new CSVReader(in);

            String[] line;

            while ((line = reader.readNext()) != null) {
                log.info("parse data...");
                String strArrayfromCSV[] = modifyStringFromCSV(line[0]);

                log.info("String converted to String array");
                //print elements of String array
                for (int i = 0; i < strArrayfromCSV.length; i++) {
                    log.info(strArrayfromCSV[i]);

                    if (strArrayfromCSV[i].contains("Ticker")) {
                        log.info("First row Header skip " + strArrayfromCSV[i]);
                    } else {

                        System.out.println("Array size " + strArrayfromCSV.length);

                        String[] rowElement = strArrayfromCSV[i].split("\t");


                        if (rowElement[0].length() > 0) {

                            if (!rowElement[0].equals("ANK1L") && !rowElement[0].equals("DKR1L")) { //todo method, that checks correct encoding for these elements

                                String dateOfSharelist = date.replace(".", "");
                                log.info("firmaId + date " + rowElement[0] + dateOfSharelist);
                                log.info("data1 Ticker " + rowElement[0]);
                                log.info("data2 firmaname " + rowElement[1]);
                                log.info("data3 ISIN " + rowElement[2]);
                                log.info("data4 Currency " + rowElement[3]);
                                log.info("data5 Market place " + rowElement[4]);
                                log.info("data6 List " + rowElement[5]);
                                log.info("data7 avg price " + rowElement[6]);
                                log.info("data8 open price " + rowElement[7]);
                                log.info("data9 high price " + rowElement[8]);
                                log.info("data10 low price " + rowElement[9]);
                                log.info("data11 last close price  " + rowElement[10]);
                                log.info("data12 last price  " + rowElement[11]);
                                log.info("data13 price change " + rowElement[12]);
                                log.info("data14 best bid " + rowElement[13]);
                                log.info("data15 best ask " + rowElement[14]);
                                log.info("data16 trades  " + rowElement[15]);
                                log.info("data17 volume " + rowElement[16]);
                                log.info("data18 turnover " + rowElement[17]);

                                FileDataDTO fileDataDTO = new FileDataDTO();
                                fileDataDTO.setId(rowElement[0] + dateOfSharelist);
                                fileDataDTO.setTicker(rowElement[0]);
                                fileDataDTO.setFirmaname(rowElement[1]);
                                fileDataDTO.setISIN(rowElement[2]);
                                fileDataDTO.setCurrency(rowElement[3]);
                                fileDataDTO.setMarketPlace(rowElement[4]);
                                fileDataDTO.setSegment(rowElement[5]);
                                fileDataDTO.setAvgPrice(rowElement[6]);
                                fileDataDTO.setLastPrice((rowElement[11]));
                                fileDataDTO.setLastClsPrice(rowElement[10]);
                                fileDataDTO.setChngPrice(rowElement[12]);
                                fileDataDTO.setBestBid(rowElement[13]);
                                fileDataDTO.setBestAsk(rowElement[14]);
                                fileDataDTO.setTrades(rowElement[15]);
                                fileDataDTO.setVolume(Long.parseLong(rowElement[16]));
                                fileDataDTO.setTurnover(rowElement[17]);
                                fileDataDTO.setDateOriginal(dateOfSharelist);
                                dataFromCSV.add(fileDataDTO);


                            }
                        }
                    }


                }
            }


        } catch (Exception a) {
            log.error("Error ", a);
            a.printStackTrace();
        }

    }


    public static void readListDataFromCSVToDb(String dateToday) {

        try {

            dataFromCSV.forEach(fileDataDTO -> {

                String today = dateToday.replace(".", "");

                try {

                    System.out.println("Lala " + fileDataDTO.getId().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (findFirmaAndSharesData(fileDataDTO) == true) {
                        log.info("UPDATE stockSharesTab");
                        updateStockShareTab(fileDataDTO);
                    }
                    if (findFirmaAndSharesData(fileDataDTO) == false) {
                        log.info("INSERT to firmaTab and StockSharesTab");
                        try {
                            insertToFirmaTab(fileDataDTO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            insertToStockShareTab(fileDataDTO);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                } catch (IOException | SQLException e) {
                    e.printStackTrace();
                }


            });

        } catch (Exception e) {
            log.error("readListDataFromCSVToDb ", e);
            e.printStackTrace();
        }
    }


}





