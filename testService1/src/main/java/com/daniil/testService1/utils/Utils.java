package com.daniil.testService1.utils;

import com.daniil.testService1.DTO.FileDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;

public class Utils {

    private static final Logger log = LoggerFactory.getLogger(Utils.class);


    public static String todayDateForURL() {

        String date = null;
        try {

            String pattern = "dd.MM.yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            SimpleDateFormat format = simpleDateFormat;
            //String oldDate =simpleDateFormat.format(new Date() );
            //String olderrDate=simpleDateFormat.format(new Date());

            date = simpleDateFormat.format(new Date());

            log.info("CHECK " + gen10Days());
        } catch (Exception e) {

            e.printStackTrace();

        }

        return date;

    }


    public static String[] modifyStringFromCSV(String original) {
        String start = original;
        String modified[] = null;

        try {

            String a = start.replaceAll("\u0000", "");
            String b = a.replaceAll("��", "");
            modified = b.split("  ");

        } catch (Exception e) {
            e.printStackTrace();
        }


        return modified;
    }


    public static LinkedHashMap gen10Days() {
        LinkedHashMap<Integer, String> dates = new LinkedHashMap<>();

        try {

            String date = null;
            for (int i = 10; dates.size() <= 10; i--) {

                String pattern = "dd.MM.yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                SimpleDateFormat format = simpleDateFormat;

                Calendar cal = new GregorianCalendar();
                cal.add(Calendar.DAY_OF_MONTH, -i);
                Date someDaysAgo = cal.getTime();


                date = simpleDateFormat.format(someDaysAgo);


                dates.put(i, date);

                System.out.println("CHECK date ago " + someDaysAgo);

            }

            log.info(dates.toString());


        } catch (Exception e) {
            log.error("error in dates ", e);
            e.printStackTrace();
        }


        return dates;
    }


    public static boolean findFirmaAndSharesData(FileDataDTO fileDataDTO) throws IOException, SQLException {
        String SQLparing = "Select b.id from STOCK_FIRMA_TAB a, STOCK_SHARE_LIST_TAB b where a.ID=b.TICKER and b.id='" + fileDataDTO.getId() + "'";
        //String SQLparing = "Select b.id from STOCK_FIRMA_TAB a, STOCK_SHARE_LIST_TAB b where a.ID=b.TICKER and b.id='122042019'";
        String vastus = "";
        boolean value = false;
        Connection con = null;
        Statement stmt = null;
        ResultSet resultSet = null;

        try {

            con = new DBConnection().getConnection();
            stmt = con.createStatement();
            resultSet = stmt.executeQuery(SQLparing);

            if (resultSet.next()) {
                log.info("TRUE");
                value = true;
                return value;
            } else {
                log.info("FALSE");
                value = false;
                return value;
            }

        } catch (SQLException e) {
            log.error("DB exception: " + e);
            vastus = e.toString();
            resultSet.close();
            stmt.close();
            con.close();
            value = false;
            return value;
        } finally {
            resultSet.close();
            stmt.close();
            con.close();
        }

    }


    public static void insertToFirmaTab(FileDataDTO fileDataDTO) throws SQLException {

        String SQLparing = "Insert into stock_firma_tab"
                + "(id, firma_name, isin, currency, marketplace)"
                + " VALUES(?,?,?,?,?)";

        Connection con = null;
        PreparedStatement preparedStatement = null;

        try {
            con = new DBConnection().getConnection();
            preparedStatement = con.prepareStatement(SQLparing);
            preparedStatement.setString(1, fileDataDTO.getTicker());
            preparedStatement.setString(2, fileDataDTO.getFirmaname());
            preparedStatement.setString(3, fileDataDTO.getISIN());
            preparedStatement.setString(4, fileDataDTO.getCurrency());
            preparedStatement.setString(5, fileDataDTO.getMarketPlace());
            int row = preparedStatement.executeUpdate();

            if (row == 1) {
                log.info("row added ok");
            } else {
                log.info("row not added NO");
            }

            preparedStatement.close();
            con.close();

        } catch (Exception e) {
            log.error("DB exception: " + e);
            preparedStatement.close();
            con.close();
        } finally {
            preparedStatement.close();
            con.close();
        }

    }

    public static void insertToStockShareTab(FileDataDTO fileDataDTO) throws SQLException {

        String SQLparing = "Insert into stock_share_list_tab"
                + "(id, ticker, last_cls_price, last_price, chng_price, best_bid, best_ask, trades, volume, turnover, date_original)"
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        Connection con = null;
        PreparedStatement preparedStatement = null;

        try {
            con = new DBConnection().getConnection();
            preparedStatement = con.prepareStatement(SQLparing);
            preparedStatement.setString(1, fileDataDTO.getId());
            preparedStatement.setString(2, fileDataDTO.getTicker());
            preparedStatement.setFloat(3, Float.parseFloat(fileDataDTO.getLastClsPrice()));
            preparedStatement.setFloat(4, Float.parseFloat(fileDataDTO.getLastPrice()));
            preparedStatement.setString(5, fileDataDTO.getChngPrice());
            preparedStatement.setFloat(6, Float.parseFloat(fileDataDTO.getBestBid()));
            preparedStatement.setFloat(7, Float.parseFloat(fileDataDTO.getBestAsk()));
            preparedStatement.setString(8, fileDataDTO.getTrades());
            preparedStatement.setFloat(9, Float.parseFloat(fileDataDTO.getVolume().toString()));
            preparedStatement.setFloat(10, Float.parseFloat(fileDataDTO.getTurnover()));
            preparedStatement.setString(11, fileDataDTO.getDateOriginal());
            int row = preparedStatement.executeUpdate();

            if (row == 1) {
                log.info("row added ok");
            } else {
                log.info("row not added NO");
            }

            preparedStatement.close();
            con.close();

        } catch (Exception e) {
            log.error("DB exception: " + e);
            preparedStatement.close();
            con.close();
        } finally {
            preparedStatement.close();
            con.close();
        }


    }

    public static void updateStockShareTab(FileDataDTO fileDataDTO) throws SQLException {

        Connection con = null;
        Statement stmt = null;
        String SQLparing = "Update stock_share_list_tab set last_price=" + Float.parseFloat(fileDataDTO.getLastPrice()) + ", " +
                "last_cls_price=" + Float.parseFloat(fileDataDTO.getLastClsPrice()) + ", chng_price='" + fileDataDTO.getChngPrice() + "', best_bid=" + Float.parseFloat(fileDataDTO.getBestBid()) + ", best_ask=" + Float.parseFloat(fileDataDTO.getBestAsk()) + ", trades=" + fileDataDTO.getTrades() + ", volume=" + Float.parseFloat(fileDataDTO.getVolume().toString()) + ", turnover=" + Float.parseFloat(fileDataDTO.getTurnover()) + "   where id='" + fileDataDTO.getId() + "'";
        try {
            con = new DBConnection().getConnection();
            stmt = con.createStatement();
            int row = stmt.executeUpdate(SQLparing);

            if (row == 1) {
                log.info("row updated OK");
            } else {
                log.info("row not updated NO");
            }

        } catch (Exception e) {
            log.error("DB exception: " + e);
            stmt.close();
            con.close();
            e.printStackTrace();
        } finally {
            stmt.close();
            con.close();
        }

    }


}
