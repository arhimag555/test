package com.daniil.testService1.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {


    public static Connection getConnection() throws IOException {

        Properties props = new Properties();
        FileInputStream fis = null;
        Connection con = null;
        try {
            fis = new FileInputStream("conf/testService1.properties");
            props.load(fis);

            // load the Driver Class
            Class.forName(props.getProperty("DBdriver"));

            // create the connection now
            con = DriverManager.getConnection(props.getProperty("DBurl"), props.getProperty("DBuser"), props.getProperty("DBpass"));

            System.out.println(con);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException z) {
            z.printStackTrace();
        }
        return con;
    }


}
