package com.daniil.testService1.controller;


import com.daniil.testService1.domain.StockDaylyViewTabEntity;
import com.daniil.testService1.repository.StockDaylyViewRepository;
import com.daniil.testService1.service.StockDaylyViewService;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class StockDaylyViewController {


    private final Logger log = LoggerFactory.getLogger(StockDaylyViewController.class);
    @Autowired
    private com.daniil.testService1.repository.StockDaylyViewRepository stockDaylyViewRepository;
    @Autowired
    private StockDaylyViewService stockDaylyViewService;


    public StockDaylyViewController(StockDaylyViewRepository stockDaylyViewRepository, StockDaylyViewService stockDaylyViewService) {
        this.stockDaylyViewRepository = stockDaylyViewRepository;
        this.stockDaylyViewService = stockDaylyViewService;
    }


    @RequestMapping(value = "/message/list_all_stocks", method = RequestMethod.GET) //using this
    public ResponseEntity<Object> showAllShares() {
        log.info("**********************<<INCOMING MESSAGE GET ALL STOCKS>>********************************");
        List<StockDaylyViewTabEntity> listShares = stockDaylyViewService.findAll();
        List<JSONObject> shares = new ArrayList<JSONObject>();

        for (StockDaylyViewTabEntity b : listShares) {
            JSONObject share = new JSONObject();
            share.put("ticker", b.getTicker());
            share.put("lastClosePrice", b.getLastClsPrice());
            share.put("lastPrice", b.getLastPrice());
            share.put("changePrice", b.getChngPrice());
            share.put("bestBid", b.getBestBid());
            share.put("bestAsk", b.getBestAsk());
            share.put("trades", b.getTrades());
            share.put("volume", b.getVolume());
            share.put("turnover", b.getTurnover());

            shares.add(share);
        }

        log.info("**********************<<RESPONSE MESSAGE SENT ALL STOCKS>>**********************************");
        return new ResponseEntity<Object>(shares, HttpStatus.OK);
    }


    @RequestMapping(value = "/list_stocks", method = RequestMethod.GET)
    public ModelAndView ListStocks(ModelAndView model) throws IOException {
        List<StockDaylyViewTabEntity> listStocks = stockDaylyViewService.findAll();
        model.addObject("listStocks", listStocks);
        model.setViewName("stocks");

        return model;
    }


//    @RequestMapping(value="/show_stocks" , method = RequestMethod.GET)
//    public ModelAndView ShowStocks(ModelAndView model, @RequestParam("originalDate") String originalDate) throws IOException{
//        List<StockDaylyViewTabEntity> showStocks = stockDaylyViewService.getByDateOriginal(originalDate);
//        model.addObject("showStocks", showStocks);
//        model.setViewName("showstocks");
//        return model;
//    }


}
