package com.daniil.testService1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class TestService1Application {

    private static final Logger log = LoggerFactory.getLogger(TestService1Application.class);


    public TestService1Application() {

    }


    public static void main(String[] args) {
        SpringApplication.run(TestService1Application.class, args);


        log.info("System Started ...");
    }


}
